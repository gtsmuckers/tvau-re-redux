place_x = room_width/2;
place_y = room_height - 150;
button_h = 24;
button_w = 256;

//controls
xx = room_width/2;
yy = room_height/2 - 50;
c_h = 32;

c_line[0] = "Move Forward: Up";
c_line[1] = "Rotate: Left & Right";
c_line[2] = "Shoot: Spacebar";
c_lines = array_length_1d(c_line);